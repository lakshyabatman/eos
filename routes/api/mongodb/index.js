const express = require('express')
const router = express.Router()
const cloudinary = require('cloudinary')
const { zip } = require('zip-a-folder')
const { mongoDump, removeDir } = require('../../../modules/mongodb-dump')
const dotEnv = require('dotenv')
dotEnv.config()

/* Config for Cloudinary API.
* More about credentials at: https://cloudinary.com/documentation/how_to_integrate_cloudinary#1_create_and_set_up_your_account
*/
cloudinary.config({
  cloud_name: process.env.EOS_CDN_NAME,
  api_key: process.env.EOS_CDN_API_KEY,
  api_secret: process.env.EOS_CDN_API_SEC
})

router.get('/', async (req, res, next) => {
  /* Connect to db based on  how we're runing EOS (dev or prod) */
  const isDev = process.env.EOS_RUN_PROJECT_AS === 'dev'

  const uri = isDev
    ? process.env.EOS_DATABASE_URI_DEV
    : process.env.EOS_DATABASE_URI_PROD

  const databaseName = isDev
    ? process.env.EOS_DATABASE_DB_DEV
    : process.env.EOS_DATABASE_DB_PROD

  /* Absolute path to our temp backup folder */
  const backupPath = './backup/dump'

  try {
    /* Takes care of dumping the collections and save them in the backupPath folder */
    const backupDbCollections = await mongoDump(uri, databaseName, backupPath)
    /* Copress the backupPath folder to an .zip in the dest directory (2nd param of the function) */
    const fileName = await ZipAFolder.main(backupPath, './backup/')

    /* Push the new generated .zip file to Cloudinary */
    return await cloudinary.v2.uploader.upload(`./backup/${fileName}`, {
      resource_type: 'raw',
      folder: 'DB',
      use_filename: true
    })
      .then(() => {
        /* It will remove the backup folder once the zip it's uploade to Cloudinary */
        removeDir('./backup/')
        /* Endoint response with the backupCollection code */
        return res.send(backupDbCollections)
      })
  } catch (error) {
    console.log('error: ', error)
  }
})

/* Generates a .zip passing a source and dest folder */
class ZipAFolder {
  static async main (src, dest) {
    const { day, month, year } = getDate()
    const fileName = `eos-${process.env.EOS_RUN_PROJECT_AS || ''}-${day}-${month}-${year}.zip`
    await zip(src, `${dest}/${fileName}`)
    return fileName
  }
}

/* Gets the dd/mm/yyyy format from current date */
const getDate = () => {
  const currentDate = new Date()
  const day = currentDate.getDate()
  const month = currentDate.getMonth() + 1
  const year = currentDate.getFullYear()

  return { day, month, year }
}

module.exports = router
